# 指針式儀表數值影像辨識

### 環境需求

Python 3.6.8 以上

### 安裝

1. 建立虛擬環境
```
python3 -m venv venv
```

2. 進入虛擬環境

Window
```
\venv\bin\activate 
```

Linux/Mac
```
source /venv/bin/activate
```

3. 環境安裝
```
pip install -f requirements.txt
```

4. 執行程式
```
python3 app.py
```


