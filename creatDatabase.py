# import packege manager

import cv2

import numpy as np
np.__version__

from matplotlib import pyplot as plt
from matplotlib import transforms
%matplotlib inline

from sklearn.decomposition import PCA
from keras.models import load_model
from sklearn.linear_model import LinearRegression
import os
import pdb

from statistics import mean
import math
import pickle

model = load_model('try_and_edit_model_CNN_2.h5')

############ function definition ############

# 灰階直方圖均化 ( 若只有一張圖片 img，則輸入 histogram([img],0) )
def histogram(frames,framenumber):
    frame_hist = frames[framenumber].copy()
    hist,bins = np.histogram(frame_hist.flatten(),256,[0,256])

    cdf = hist.cumsum()
    cdf_normalized = cdf * hist.max()/cdf.max()

    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max() - cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')

    frame_hist = cdf[frame_hist]
    
    return frame_hist

#找可能為物件（指標、刻度、數字）的輪廓
def find_obj_contours(frames,framenumber, canny_param1, canny_param2):
    # Input image, Canny's param1, param2, Area lower bound, Area upper bound
    
    frame = frames[framenumber].copy()
    
    # ------- 邊緣偵測 -------
    canny = cv2.Canny(frame, canny_param1, canny_param2)
    
    # ------- 找輪廓 -------
    ( cnts , __ ) = cv2.findContours(canny, 
                                    cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)

    # loop over the contours individually
    contour_objs = []
    for idx, c in enumerate(cnts):
        contour_objs.append(c)
    
    return contour_objs

# 過濾大小
def get_larger_area(cnts,area_lb):
    contour_objs = []
    for idx, c in enumerate(cnts):
        if (cv2.contourArea(c) > area_lb):
            contour_objs.append(c)
    return contour_objs

# 找類似數字的輪廓(為之後使用方便，範圍皆為正方形)
def get_numberlike_contours(frames,framenumber,cnts):
    frame = frames[framenumber].copy()
    num_range = []
    num_contour=[]
    for tar_contour_obj in cnts[framenumber]:
        pts_data = tar_contour_obj.reshape(np.shape(tar_contour_obj)[0], np.shape(tar_contour_obj)[2])

        box_min_x = np.min(pts_data[:, 0])
        box_max_x = np.max(pts_data[:, 0])
        box_min_y = np.min(pts_data[:, 1])
        box_max_y = np.max(pts_data[:, 1])

        box_min_x = int(max( box_min_x - (box_max_x - box_min_x) * 0.2, 0 ))
        box_max_x = int(min( box_max_x + (box_max_x - box_min_x) * 0.2, frame.shape[1] ))
        box_min_y = int(max( box_min_y - (box_max_y - box_min_y) * 0.2, 0 ))
        box_max_y = int(min( box_max_y + (box_max_y - box_min_y) * 0.2, frame.shape[0] ))

        # 去掉輪廓為一條鉛錘線或水平線的物件 以及 篩選較高的物件(因為數字是高的)
        if (box_max_y - box_min_y > 0) and (box_max_x - box_min_x > 0) \
                                and ((box_max_y - box_min_y)/(box_max_x - box_min_x) > 1):
            side_len = (box_max_y - box_min_y)/2
            xm = mean([box_max_x,box_min_x])
            ym = mean([box_max_y,box_min_y])
            
            box_min_x = int(math.ceil(xm - side_len))
            box_max_x = int(math.floor(xm + side_len))
            box_min_y = int(math.ceil(ym - side_len))
            box_max_y = int(math.floor(ym + side_len))

            # 取正方形的範圍，以便之後 train
            if (box_min_x >= 0) and (box_max_x <= frame.shape[1]) and (box_min_y >= 0) and (box_max_y <= frame.shape[0]):
                num_contour.append(tar_contour_obj)
                num_range.append([box_min_x, box_max_x, box_min_y, box_max_y])
            
    return [num_range,num_contour]

# 人工判斷哪些輪廓是數字，並挑出。(picked_index)
def pick_cnts(frames,framenumber,cnts,picked_index):
    num_range = []
    num_contour = []
    numberlike_contours = get_numberlike_contours(frames,framenumber,cnts)
    for ind in picked_index:
        num_range.append(numberlike_contours[0][ind])
        num_contour.append(numberlike_contours[1][ind])
    return [num_range, num_contour]

def to_label(num_range,labels):
    range_labeled = []
    for i,label in enumerate(labels):
        range_labeled.append([label,num_range[i]])
    return range_labeled

# 將鐘面上的數字剪下，分別蒐集成 MNIST 的形態
# range_labeled 的元素應該長這樣:[recog_num, [box_min_x, box_max_x, box_min_y, box_max_y]]
def meter_database(frames,framenumber,ranges_labeled):
    x = []
    y = []
    for recog_num, [box_min_x, box_max_x, box_min_y, box_max_y] in ranges_labeled[framenumber]:
        num_box = frames[framenumber][box_min_y:box_max_y, box_min_x:box_max_x].copy()
        num_box = histogram([num_box],0)
        num_box = 255 - np.array(num_box) # MNIST 是黑底白字
        num_box = cv2.resize(num_box, (28, 28)) # MNIST data 的尺寸
        x.append(num_box)
        y.append(recog_num)
    x = np.array(x)
    y = np.array(y).astype('uint8')
    return [x,y]

# 觀察圖片中的所有
def observe_cnts(frames,framenumber,cnts,columns,hieght):
    rows = math.ceil(len(cnts[framenumber])/columns)
    fig = plt.figure(figsize=(25, hieght))
    ax=[]
    
    for i in range(len(cnts[framenumber])):
        frame = frames[framenumber].copy()
        cv2.drawContours(frame, cnts[framenumber], i, (255, 0, 0), 1)
        ax.append( fig.add_subplot(rows, columns, i+1) )
        ax[-1].set_title(str(i)+"-th : "+str(len(cnts[framenumber][i]))+" pixels\narea: "+str(cv2.contourArea(cnts[framenumber][i])))
        plt.imshow(frame, cmap=plt.get_cmap('gray'),vmin=0,vmax=255)

# 只取某個區域內的輪廓
def cnts_in_some_region(cnts,x_lb,x_ub,y_lb,y_ub):
    restricted_cnts =[]
    for cnt in cnts:
        temp = cnt.reshape(cnt.shape[0],cnt.shape[2])
        x_min = np.min(temp[:,0])
        x_max = np.max(temp[:,0])
        y_min = np.min(temp[:,1])
        y_max = np.max(temp[:,1])
        if (x_min > x_lb) and (x_max < x_ub) and (y_min > y_lb) and (y_max < y_ub):
            restricted_cnts.append(cnt)
    return restricted_cnts

# 將同一張 frame 的各個輪廓分開顯示
def display_cnts(frames,framenumber,num_ranges,num_contours,hieght):
    columns = 8
    rows = math.ceil(len(num_contours[framenumber])/columns)
    fig = plt.figure(figsize=(25, hieght))
    ax=[]
    
    for i in range(len(num_contours[framenumber])):
        
        frame = frames[framenumber].copy()
        cv2.drawContours(frame, num_contours[framenumber], i, (255, 0, 0), 1)
        
        box_min_x, box_max_x, box_min_y, box_max_y = num_ranges[framenumber][i]
        
        num_box = frame[box_min_y:box_max_y, box_min_x:box_max_x]
        X_input = cv2.resize(num_box, (28, 28))
        _,X_input = cv2.threshold(X_input,100,255,cv2.THRESH_BINARY_INV) # 弄成黑底白字藉以模仿 MNIST 資料庫內的資料
        X_input = X_input.reshape(1,28,28,1)/255
        y_output = model.predict(X_input)
        recog_num = np.argmax(y_output)
        
        cv2.rectangle(frame, 
                      (box_min_x, box_min_y), 
                      (box_max_x, box_max_y), 
                      color=(0, 255, 0), 
                      thickness=2)
        cv2.line(frame, 
                 (box_max_x, box_min_y), 
                 (box_min_x, box_max_y), 
                 color=(0, 255, 0),
                 thickness=1)
        cv2.line(frame, 
                 (box_max_x, box_max_y), 
                 (box_min_x, box_min_y), 
                 color=(0, 255, 0), 
                 thickness=1)

        font                   = cv2.FONT_HERSHEY_SIMPLEX
        bottomLeftCornerOfText = (box_max_x, box_max_y)
        fontScale              = 1
        fontColor              = (255, 0, 255)
        lineType               = 2

        cv2.putText(frame, str(recog_num),
                    bottomLeftCornerOfText,
                    font, 
                    fontScale,
                    fontColor,
                    lineType)

        ax.append( fig.add_subplot(rows, columns, i+1) )
        ax[-1].set_title(str(i)+"-th in frame "+str(framenumber)+" : "+str(recog_num))
        plt.imshow(frame, cmap=plt.get_cmap('gray'),vmin=0,vmax=255)

# 顯示人工標紀的結果
def display_labels(frames,framenumber,ranges_labeled,hieght):
    columns = 8
    rows = math.ceil(len(ranges_labeled[framenumber])/columns)
    fig = plt.figure(figsize=(25, hieght))
    ax=[]
    
    for i in range(len(ranges_labeled[framenumber])):
        
        frame = frames[framenumber].copy()
        
        label,[box_min_x, box_max_x, box_min_y, box_max_y] = ranges_labeled[framenumber][i]
        
        num_box = frame[box_min_y:box_max_y, box_min_x:box_max_x]
        
        cv2.rectangle(frame, 
                      (box_min_x, box_min_y), 
                      (box_max_x, box_max_y), 
                      color=(0, 255, 0), 
                      thickness=2)
        cv2.line(frame, 
                 (box_max_x, box_min_y), 
                 (box_min_x, box_max_y), 
                 color=(0, 255, 0),
                 thickness=1)
        cv2.line(frame, 
                 (box_max_x, box_max_y), 
                 (box_min_x, box_min_y), 
                 color=(0, 255, 0), 
                 thickness=1)

        font                   = cv2.FONT_HERSHEY_SIMPLEX
        bottomLeftCornerOfText = (box_max_x, box_max_y)
        fontScale              = 1
        fontColor              = (255, 0, 255)
        lineType               = 2

        cv2.putText(frame, str(label),
                    bottomLeftCornerOfText,
                    font, 
                    fontScale,
                    fontColor,
                    lineType)

        ax.append( fig.add_subplot(rows, columns, i+1) )
        ax[-1].set_title(str(i)+"-th in frame "+str(framenumber)+" : "+str(label))
        plt.imshow(frame, cmap=plt.get_cmap('gray'),vmin=0,vmax=255)

############ 讀取圖片 ############
frames = []
for s in [3,4,5]:
    for t in range(10):
        img = np.load('/home/shared_documents/P2019_LEVELING/Dataset/daily/daily_right/20191101_09'+str(s)+str(t)+'04.npy')
        img_rotated = np.empty([img.shape[1],img.shape[0]])
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                img_rotated[j][i]=img[i][-j]
        img_rotated = img_rotated.astype(np.uint8) # 下面 img = cdf[img] 需要用到
        frames.append(img_rotated)

############ 灰階直方圖均化 ############
frames_hist = []
for frame in frames:
    frames_hist.append(histogram([frame],0))

############ 所有可能的輪廓 ############
contour_objs = []
for i in range(len(frames_hist)):
    contour_objs.append( find_obj_contours(frames_hist, i, canny_param1=100, canny_param2=200) )

############ 所有可能的輪廓-篩 ############
nonpt_cnts = []
for i in range(len(contour_objs)):
    nonpt_cnts.append( get_larger_area(contour_objs[i],100) )

############ 數字的可能輪廓 ############
numlike_ranges = []
numlike_contours = []
for i in range(len(nonpt_cnts)):
    numlike_ranges.append([])
    numlike_contours.append([])
    numlike_ranges[-1], numlike_contours[-1] = get_numberlike_contours(frames,i,nonpt_cnts)

picked_indices = [[]]*len(frames)
picked_indices[0] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[1] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[2] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[3] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[4] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[5] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[6] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[7] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[8] = [0,1,2,3,4,5,7,8,10,11,12,13,14]
picked_indices[9] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[10] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[11] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[12] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[13] = [0,1,2,3,4,5,7,8,10,11,12,13,14]
picked_indices[14] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[15] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[16] = [0,1,2,3,4,5,7,9,10,11,12,13,14]
picked_indices[17] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[18] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[19] = [0,1,2,3,12,4,6,7,8,9,10,11,13]
picked_indices[20] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[20] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[21] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[22] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[23] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[24] = [0,1,2,3,14,4,6,7,9,10,11,12,13]
picked_indices[25] = [0,13,1,2,3,4,6,7,8,9,10,11,12]
picked_indices[26] = [0,1,2,3,4,5,7,8,10,11,12,13,14]
picked_indices[27] = [0,1,2,3,4,6,8,9,10,11,12,13,14]
picked_indices[28] = [0,1,2,3,4,5,7,8,9,10,11,12,13]
picked_indices[29] = [0,1,2,3,4,5,7,8,10,11,12,13,14]

############ 數字輪廓 ############
num_ranges = []
num_contours = []
for framenumber, picked_index in enumerate(picked_indices):
    num_ranges.append([])
    num_contours.append([])
    num_ranges[-1], num_contours[-1] = pick_cnts(frames,framenumber,nonpt_cnts,picked_index)

############ 標記 ############
ranges_labeled = [[]]*len(frames)
for i in range(len(num_ranges)):
    ranges_labeled[i] = to_label(num_ranges[i],[abs(6-i) for i in range(13)])

############ 蒐集成 MNIST 的形態 ############
images, labels = meter_database(frames,0,ranges_labeled)
for i in range(len(frames)-1):
    image, label = meter_database(frames,i+1,ranges_labeled)
    images = np.vstack([images,image])
    labels = np.hstack([labels,label])

with open("num_on_meter_20191129_11.txt","wb") as f:
    pickle.dump([images,labels],f)